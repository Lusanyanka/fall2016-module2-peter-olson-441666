<!DOCTYPE html>
<html>
<head>
	<title>Calculator</title>
	<link rel="stylesheet" type="text/css" href="calc.css"/>
</head>
<!-- Scripting starts -->
<body>
	<div id="calcBox">
		<div id="resultsBox">
		<?php
		$op1 = $_GET['o1'];
		$op2 = $_GET['o2'];
		$op = $_GET['op'];
		if(is_null($op1) || is_null($op2)){
			echo "<p id=waiting>Enter numbers for the operands and select an operation.</p>\n";
		}
		else{
			global $op1, $op2, $op;
			$opchar = NULL;
			$result = 0;

			function printError($e){
				if($e == 0){
					echo "<p id=error>Please do not divide by zero!</p>\n";
				}
				else if($e == 1){
					echo "<p id=error>Please input numbers.</p>\n";
				}
			}

			function printResult($r){
				global $op1, $op2, $opchar;
				if(is_int($r)){
					printf("<p id=output>%d %s %d = %d</p>\n", $op1, $opchar, $op2, $r);
				}
				else{
					printf("<p id=output>%.3f %s %.3f = %.3f</p>\n", $op1, $opchar, $op2, $r);
				}
			}

			/* Assign the right char for the op */
			switch($op){
				case 'a':
				$opchar = '+';
				break;
				case 's':
				$opchar = '-';
				break;
				case 'm':
				$opchar = '*';
				break;
				case 'd':
				$opchar = '/';
				break;
			}

			/* Check if they're numbers and do math */
			if(!is_numeric($op1) && !is_numeric($op2)){
				printError(1);
			}
			else if($op == 'a'){
				printResult($op1 + $op2);
			}
			else if($op == 's'){
				printResult($op1 - $op2);
			}
			else if($op == 'm'){
				printResult($op1 * $op2);
			}
			else{
				if($op2 == 0){
					echo printError();
				}
				else{
					echo printResult($op1/$op2);
				}
			}
		}
		?>
		</div>
		<div id="errorBox">
		</div>
		<form action="calc.php" method="GET">
			<label><input class="textInput" placeholder="Left Operand" type="text" name="o1" /></label>
			<label><input class="textInput" placeholder="Right Operand" type="text" name="o2" /></label>
			<!-- Operations -->
			<input class="radioInput" type="radio" name="op" id="addition" value="a" checked>
			<label for="addition">Addition</label><br>
			<input class="radioInput" type="radio" name="op" id="subtraction" value="s">
			<label for="subtraction">Subtraction</label><br>
			<input class="radioInput" type="radio" name="op" id="multiplication" value="m">
			<label for="multiplication">Multiplication</label><br>
			<input class="radioInput" type="radio" name="op" id="division" value="d">
			<label for="division">Division</label><br>
			<!-- Submission and reset -->
			<div>
				<input type="submit" value="Equals">
				<input type="reset" value="Clear">
			</div>
		</form>
	</div>
</body>
</html>
